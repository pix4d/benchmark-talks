#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>

#include "heap.hpp"
#include "rand_vec.hpp"

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << "usage " << *argv << " <vector-size> " << std::endl;
        std::exit(EXIT_FAILURE);
    }

    auto size = std::atoi(argv[1]);

    auto vec = generate_vec(size);

    std::size_t comps = heap_sort(vec);

    assert(std::is_sorted(std::begin(vec), std::end(vec)));

    std::cout << "successfully sorted " << size << " elements with " << comps << " comparisons.\n";
}
