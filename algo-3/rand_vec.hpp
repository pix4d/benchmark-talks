#pragma once

#include <vector>
#include <cstdlib>

inline std::vector<int> generate_vec(size_t num)
{
    std::vector<int> retval;

    retval.reserve(num);

    while (retval.size() < num) {
        retval.push_back(int(std::rand()));
    }

    return retval;
}

inline std::vector<int> generate_sorted(size_t num)
{
    std::vector<int> retval;

    retval.reserve(num);

    int cnt = 0;
    while (retval.size() < num) {
        cnt += int(std::rand()) % 10;
        retval.push_back(cnt);
    }

    return retval;
}
