#include <vector>
#include "heap.hpp"

inline std::size_t parent(std::size_t i)
{
    return (i - 1) / 2;
}

inline std::size_t left_child(std::size_t i)
{
    return 2 * i + 1;
}

inline std::size_t right_child(std::size_t i)
{
    return 2 * i + 2;
}

std::size_t max_heapify(std::vector<int> &v, std::size_t i, std::size_t heap_size)
{
    auto l = left_child(i);
    auto r = right_child(i);

    auto largest = i;
    auto comparisons = 0ul;

    if (l < heap_size && v[largest] < v[l]) {
        largest = l;
    }
    comparisons++;

    if (r < heap_size && v[largest] < v[r]) {
        largest = r;
    }
    comparisons++;

    if (largest != i) {
        std::swap(v[largest], v[i]);
        return comparisons + max_heapify(v, largest, heap_size);

    } else {
        return comparisons;
    }
}

std::size_t build_max_heap(std::vector<int> &v)
{
    auto comparisons = 0ul;

    for (int i = int(v.size()) / 2; i > -1; i--) {
        comparisons += max_heapify(v, i, v.size());
    }

    return comparisons;
}

std::size_t heap_sort(std::vector<int> &v)
{
    auto comparisons = build_max_heap(v);

    for (auto i = v.size() - 1; i > 0; i--) {
        std::swap(v[0], v[i]);
        comparisons += max_heapify(v, 0, i);
    }

    return comparisons;
}
