#pragma once

#include <vector>

std::size_t max_heapify(std::vector<int> &v, std::size_t i, std::size_t heap_size);

std::size_t build_max_heap(std::vector<int> &v);

std::size_t heap_sort(std::vector<int> &v);
