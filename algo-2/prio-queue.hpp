#pragma once

class PriorityQueue {
public:

    int max() const;

    int pop_max() const;

    void increase_key(std::size_t which, int new_key);

    void insert(int key);
};
