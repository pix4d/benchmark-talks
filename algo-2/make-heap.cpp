#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>

#include "heap.hpp"
#include "rand_vec.hpp"

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << "usage " << *argv << " <heap-size> " << std::endl;
        std::exit(EXIT_FAILURE);
    }

    auto size = std::atoi(argv[1]);

    auto vec = generate_vec(size);

    auto comp = build_max_heap(vec);

    // we actually have heap stuff in <algorithm> \o/
    assert(std::is_heap(std::begin(vec), std::end(vec)));

    std::cout << "successfully constructed a " << size << " element heap with " << comp << " comparisons " << std::endl;
}
