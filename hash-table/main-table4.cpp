#include <iostream>
#include <assert.h>

#include "Table4.hpp"
#include "randomStringGen.hpp"

// useful values 64, 1024, 2056, 4096
static const size_t INSERTIONS = 1024;

static const size_t SEARCHES = 2056;

static const size_t RUNS = 2056;

static const size_t BUCKETS = 128;

int main(int argc, char **argv)
{
    double collision_fraction = 0.0;

    StringProperties props;

    for (size_t run = 0; run < RUNS; ++run) {

        FileHashMap map(BUCKETS);
        std::vector<std::array<char, 64>> storage(INSERTIONS);
        auto payload_pos = &storage[0];

        std::vector<std::string> strs;
        strs.reserve(INSERTIONS);

        size_t inserts = INSERTIONS;

        // first do some insertions so we can do some finds
        while (inserts > (INSERTIONS - INSERTIONS / 10)) {

            strs.push_back(get_next(props));
            map.insert(strs.back(), FileMetadata(payload_pos++));

            inserts--;
        }

        size_t searches = SEARCHES;

        // now do the remaining work
        while (inserts && searches) {

            if (rand() % 2 && inserts) {
                strs.push_back(get_next(props));
                map.insert(strs.back(), FileMetadata(payload_pos++));
                inserts--;

            } else if (searches) {
                assert(map.find(strs[rand() % strs.size()]));
                searches--;
            }
        }

        collision_fraction += static_cast<double>(map.collisions());
    }


    auto total_inserts = static_cast<double>(RUNS * INSERTIONS);

    std::cout << "collision_fraction: " << (collision_fraction / total_inserts) << std::endl;
    return 0;
}
