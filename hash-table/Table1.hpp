#pragma once

/// Table1 - the basic simple implementation

#include <utility>
#include <vector>
#include <string>
#include <cstdint>
#include <array>

using hash_t = std::uint16_t;

static constexpr hash_t HASH_UBIT_MASK{1u << 15};

hash_t rotate(hash_t h)
{
    return ((HASH_UBIT_MASK & h) ? (0x1) : (0x0)) | (h << 1);
}

hash_t hash(const std::string &s)
{
    hash_t h{0u};

    for (auto c : s) {
        h = h ^ rotate(h) ^ c;
    }

    return h;
}

struct FileMetadata
{
    FileMetadata() = default;

    FileMetadata(FileMetadata &&) = default;

    FileMetadata(const FileMetadata &) = default;

    std::array<char, 64> dummy_payload;
};

class FileHashMap {
public:

    explicit FileHashMap(size_t buckets = 128)
        : m_buckets(buckets)
    {
        // for (auto &v : m_buckets) {
        //     m_buckets.reserve(32);
        // }
    }

    /// insert metadata object at str key, complexity: ammortized O(1).
    void insert(const std::string &str, FileMetadata &&data);

    /// find metadata object at str key, complexity: ammortized O(1).
    const FileMetadata* find(const std::string &str) const;

    /// find number of collisions in table. complexity O(n).
    size_t collisions() const;
private:

    struct Entry {

        std::string key;

        FileMetadata data;
    };

    using Bucket = std::vector<Entry>;

    std::vector<Bucket> m_buckets;
};

void FileHashMap::insert(const std::string &str, FileMetadata &&data)
{
    hash_t h = hash(str);

    size_t pos = size_t(h % m_buckets.size());

    m_buckets[pos].push_back({str, std::move(data)});
}

const FileMetadata* FileHashMap::find(const std::string &str) const
{
    hash_t h = hash(str);

    size_t pos = size_t(h % m_buckets.size());

    auto &bucket = m_buckets[pos];

    for (auto it = bucket.begin(); it != bucket.end(); ++it) {
        if (str == it->key) {
            return &it->data;
        }
    }

    return nullptr;
}

size_t FileHashMap::collisions() const
{
    size_t ret = 0u;
    for (const Bucket &b : m_buckets) {
        if (b.size() > 1u) {
            ret += b.size() - 1u;
        }
    }

    return ret;
}
