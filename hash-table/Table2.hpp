#pragma once

/// Table2 - vs Table1 we now store the data payload separately

#include <utility>
#include <vector>
#include <string>
#include <cstdint>
#include <array>

using hash_t = std::uint16_t;

static constexpr hash_t HASH_UBIT_MASK{1u << 15};

hash_t rotate(hash_t h)
{
    return ((HASH_UBIT_MASK & h) ? (0x1) : (0x0)) | (h << 1);
}

hash_t hash(const std::string &s)
{
    hash_t h{0u};

    for (auto c : s) {
        h = h ^ rotate(h) ^ c;
    }

    return h;
}

struct FileMetadata
{
    FileMetadata() = default;

    FileMetadata(FileMetadata &&) = default;

    FileMetadata(const FileMetadata &) = default;

    std::array<char, 64> dummy_payload;
};

class FileHashMap {
public:

    explicit FileHashMap(size_t buckets = 128)
        : m_keys(buckets)
        , m_vals(buckets)
    { }

    /// insert metadata object at str key, complexity: ammortized O(1).
    void insert(const std::string &str, FileMetadata &&data);

    /// find metadata object at str key, complexity: ammortized O(1).
    const FileMetadata* find(const std::string &str) const;

    /// find number of collisions in table. complexity O(n).
    size_t collisions() const;
private:

    std::vector<std::vector<std::string>> m_keys;

    std::vector<std::vector<FileMetadata>> m_vals;
};

void FileHashMap::insert(const std::string &str, FileMetadata &&data)
{
    hash_t h = hash(str);

    size_t pos = size_t(h % m_keys.size());

    m_keys[pos].push_back(str);
    m_vals[pos].push_back(std::move(data));
}

const FileMetadata* FileHashMap::find(const std::string &str) const
{
    hash_t h = hash(str);

    size_t pos = size_t(h % m_keys.size());

    auto &kbucket = m_keys[pos];

    for (auto it = kbucket.begin(); it != kbucket.end(); ++it) {
        if (str == *it) {
            auto idx = it - kbucket.begin();

            return &m_vals[pos][idx];
        }
    }

    return nullptr;
}

size_t FileHashMap::collisions() const
{
    size_t ret = 0u;
    for (const auto &b : m_keys) {
        if (b.size() > 1u) {
            ret += b.size() - 1u;
        }
    }

    return ret;
}
