#pragma once

#include <string>
#include <stdlib.h>

struct StringProperties {

    size_t max_len = 32;

};

std::string get_next(const StringProperties &props)
{
    size_t len = rand() % props.max_len;

    std::string ret;
    ret.reserve(len);

    char a = rand() % 4u;
    char b = rand() % 255u;
    char c = b;

    while (len-- != 0u) {
        ret.push_back(c);
        c = c << a;
        c ^= b;
    }

    return ret;
}
