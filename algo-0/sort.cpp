#include <algorithm>
#include <iostream>
#include <cassert>
#include <cstring>

#include "rand_vec.hpp"

/// @brief insertion-sort parameter v in-place
/// @return number of comparisons done
size_t insertion_sort(std::vector<int> &v);

/// @brief merge-sort parameter v in-place
/// @return number of comparisons done
size_t merge_sort(std::vector<int> &v);

static const char * const SORT_TYPES = "{\"insertion\", \"merge\"}";

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cerr << "usage " << *argv << "<sort-type> <vector-size>\n"
            "where <sort-type> is one of " << SORT_TYPES << '\n';
        std::exit(EXIT_FAILURE);
    }

    auto sort_function = [&]() -> decltype(&insertion_sort)
        {
            if (!std::strcmp("insertion", argv[1])) {
                return &insertion_sort;

            } else if (!std::strcmp("merge", argv[1])) {
                return &merge_sort;

            } else {
                return nullptr;
            }
        }();

    if (!sort_function) {
        std::cerr << "unkown sort type \"" << argv[1] << "\" - pick one of " << SORT_TYPES << '\n';
        std::exit(EXIT_FAILURE);
    }

    auto size = std::atoi(argv[2]);

    auto vec = generate_vec(size);

    size_t comps = sort_function(vec);

    assert(std::is_sorted(std::begin(vec), std::end(vec)));

    std::cout << "successfully sorted " << size << " elements with " << comps << " comparisons.\n";
}


size_t insertion_sort(std::vector<int> &v)
{
    size_t comparisons = 0;

    auto begin = v.begin();
    auto end = v.end();
    auto sorted = end;

    while (sorted > begin) {

        auto to_insert = *(sorted-1);
        auto pos = sorted;

        while (pos < end && *pos < to_insert) {
            *(pos-1) = *pos;
            pos++;

            comparisons++;
        }

        if (pos < end) {
            comparisons++;
        }

        *(pos-1) = to_insert;
        sorted--;
    }

    return comparisons;
}

template < typename Iter >
size_t merge_sort_impl(Iter begin, Iter mid, Iter end, Iter scratch)
{
    if (begin == mid || mid == end) {
        return 0;
    }

    auto size_left = mid - begin;
    size_t comp_left = merge_sort_impl(begin, begin + size_left / 2, mid, scratch);

    auto size_right = end - mid;
    size_t comp_right = merge_sort_impl(mid, mid + size_right / 2, end, scratch);

    auto lowest_left = begin;

    auto lowest_right = mid;

    size_t comparisons = 0;
    auto dst = scratch;
    while (lowest_left < mid && lowest_right < end) {

        if (*lowest_left < *lowest_right) {
            *dst = *lowest_left;
            lowest_left++;
            dst++;

        } else {
            *dst = *lowest_right;
            lowest_right++;
            dst++;
        }

        comparisons++;
    }

    // deal with the remaining copies if needed
    // std::copy deals with empty ranges, but this is for clarity
    if (lowest_left < mid) {
        std::copy(lowest_left, mid, dst);

    } else {
        std::copy(lowest_right, end, dst);
    }

    // now copy back
    auto size = end - begin;
    std::copy(scratch, scratch + size, begin);

    return comparisons + comp_left + comp_right;
}

size_t merge_sort(std::vector<int> &v)
{
    std::vector<int> scratch(v.size());

    return merge_sort_impl(v.begin(), v.begin() + v.size()/2, v.end(), scratch.begin());
}
