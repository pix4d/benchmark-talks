# Simple binary search tree implementation together
# with pre-, in- and postorder traversals

class Node:
    def __init__(self, key, left, right):
        self.key = key
        self.left = left
        self.right = right

    def search(self, key):
        if key == self.key:
            return self

        elif key < self.key:
            return self.left.search(key) if self.left != None else None

        elif key > self.key:
            return self.right.search(key) if self.right != None else None

class BSTree:

    def __init__(self):
        self.root = None

    def search(self, key):
        if self.root is None:
            return None
        else:
            return self.root.search(key)

    def insert(self, key):
        if self.root  is None:
            self.root = Node(key, None, None)

        else:
            not_done = False
            p = self.root

            while not_done:
                if p.key < key:
                    if p.left is None:
                        # insert
                        p.left = Node(key, None, None)

                    else:
                        # go left
                        p = p.left

                else:
                    if p.right is None:
                        # insert
                        p.right = Node(key, None, None)

                    else:
                        # go right
                        p = p.right

    def delete(self, Node):
        # find rightmost left descendant
        pass


def preoder_traverse(root, visitor):
    if root is None:
        return

    visitor(root)
    preorder_traverse(root.left, visitor)
    preorder_traverse(root.right, visitor)

# Note: "BST sort" n inserts followed by inorder_print
def inorder_traverse(root, visitor):
    if root is None:
        return

    inorder_traverse(root.left, visitor)
    visitor(root)
    inorder_traverse(root.right, visitor)

def postorder_traverse(root, visitor):
    if root is None:
        return

    postorder_traverse(root, visitor)
    postorder_traverse(root, visitor)
    visitor(root)
