#pragma once

#include <utility>

template < typename IterType, typename ValType >
std::pair<std::size_t, IterType> linear_search(IterType begin, IterType end, ValType v)
{
    auto it = begin;
    std::size_t comparisons = 0;
    for ( ; it != end && v != *it; ++it) {
        comparisons++;
    }

    return { comparisons, it };
}

template < typename IterType, typename ValType >
std::pair<std::size_t, IterType> binary_search(IterType begin, IterType end, ValType v)
{
    auto low = begin;
    auto high = end;

    std::size_t comparisons = 0;

    while (high - low > 1) {
        auto size = (high - low);
        auto mid = low + size / 2;

        if (*mid < v) {
            low = mid;
            comparisons++;

        } else if (*mid == v) {
            comparisons++;
            return { comparisons, mid };

        } else {
            high = mid;
        }
    }

    return { comparisons, (*low == v) ? low : end };
}
