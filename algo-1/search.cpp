#include <cstdlib>
#include <cstring>
#include <cassert>

#include <iostream>
#include <algorithm>

#include "search.hpp"
#include "rand_vec.hpp"

static const char * const SEARCH_TYPES = "{\"linear\", \"binary\"}";

std::vector<int> generate_searches(const std::vector<int> &v, std::size_t searches);

int main(int argc, char **argv)
{
    if (argc != 4) {
        std::cerr << "usage " << *argv << " <search-strategy> <vector-size> <searches>" <<
            "where <sort-type> is one of " << SEARCH_TYPES << std::endl;
        std::exit(EXIT_FAILURE);
    }

    auto size = std::atoi(argv[2]);
    auto search_size = std::atoi(argv[3]);

    if (!std::strcmp("linear", argv[1])) {
        auto vec = generate_vec(size);
        auto searches = generate_searches(vec, search_size);
        std::size_t comparisons = 0;

        for (auto s : searches) {
            auto res = ::linear_search(std::begin(vec), std::end(vec), s);
            assert(res.second != std::end(vec)); // assert successful find
            comparisons += res.first;
        }

        std::cout << "successfully found " << search_size <<  " elements with " << comparisons << " comparisons " << std::endl;

    } else if (!std::strcmp("binary", argv[1])) {
        auto vec = generate_sorted(size);
        auto searches = generate_searches(vec, search_size);
        std::size_t comparisons = 0;

        for (auto s : searches) {
            auto res = ::binary_search(std::begin(vec), std::end(vec), s);
            assert(res.second != std::end(vec)); // assert successful find
            comparisons += res.first;
        }

        std::cout << "successfully found " << search_size <<  " elements with " << comparisons << " comparisons " << std::endl;

    } else {
        std::cerr << "unkown search type \"" << argv[1] << "\" - pick one of " << SEARCH_TYPES << '\n';
        std::exit(EXIT_FAILURE);
    }
}

std::vector<int> generate_searches(const std::vector<int> &v, std::size_t searches)
{
    std::vector<int> retval;
    retval.reserve(searches);

    while (retval.size() < searches) {
        retval.push_back(v[std::rand() % (v.size() - 1)]);
    }

    return retval;
}
